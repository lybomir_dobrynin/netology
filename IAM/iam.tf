resource "yandex_iam_service_account" "sa" {
  name = "sa"
}

resource "yandex_resourcemanager_folder_iam_binding" "sa_bind" {
  role         = "editor"
  folder_id    = var.yandex_folder_id
  members      = [
    "serviceAccount:${yandex_iam_service_account.sa.id}",
  ]
}

resource "yandex_iam_service_account_iam_member" "admin-account-iam" {
  service_account_id = "${yandex_iam_service_account.sa.id}"
  role               = "storage.editor"
  member             = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa.id
  description        = "Static access key for object storage"
}

resource "yandex_iam_service_account_key" "default" {
  service_account_id = "${yandex_iam_service_account.sa.id}"
  description        = "IAM access key for other"
}

resource "local_file" "default" {
  content = try(jsonencode({
    created_at         = yandex_iam_service_account_key.default.created_at
    id                 = yandex_iam_service_account_key.default.id
    key_algorithm      = yandex_iam_service_account_key.default.key_algorithm
    private_key        = yandex_iam_service_account_key.default.private_key
    public_key         = yandex_iam_service_account_key.default.public_key
    service_account_id = yandex_iam_service_account_key.default.service_account_id
  }), null)
  filename = "../Infrastructure/key.json"
}