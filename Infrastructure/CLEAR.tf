resource "null_resource" "example2" {
  provisioner "local-exec" {
    command = <<EOT
      ssh-keygen -f "/root/.ssh/known_hosts" -R "nowiknowit.ru"
      ssh-keygen -f "/root/.ssh/known_hosts" -R "db01.nowiknowit.ru"
      ssh-keygen -f "/root/.ssh/known_hosts" -R "db02.nowiknowit.ru"
      ssh-keygen -f "/root/.ssh/known_hosts" -R "app.nowiknowit.ru"
      ssh-keygen -f "/root/.ssh/known_hosts" -R "gitlab.nowiknowit.ru"
      ssh-keygen -f "/root/.ssh/known_hosts" -R "runner.nowiknowit.ru"
      ssh-keygen -f "/root/.ssh/known_hosts" -R "monitoring.nowiknowit.ru"
    EOT
  } 
}