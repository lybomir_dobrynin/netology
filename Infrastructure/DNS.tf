resource "yandex_dns_zone" "nowiknowit-ru" {
  name    = "nowiknowit-ru"
  zone    = "nowiknowit.ru."
  public  = true
}

resource "yandex_dns_recordset" "record-set" {
  for_each = toset(["www.", "gitlab.", "grafana.", "alertmanager.", "prometheus.",""])

  zone_id = yandex_dns_zone.nowiknowit-ru.id
  name    = "${each.key}nowiknowit.ru."
  type    = "A"
  ttl     = 200
  data    = [
    yandex_compute_instance.nginx.network_interface.0.nat_ip_address
  ]
  depends_on = [
    yandex_vpc_address.nginx-public-ip
  ]
}