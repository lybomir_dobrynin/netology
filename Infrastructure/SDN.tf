resource "yandex_vpc_network" "default" {
  name = "net"
}

resource "yandex_vpc_subnet" "default" {
  name           = "subnet"
  zone           = var.yandex_zone
  network_id     = yandex_vpc_network.default.id
  v4_cidr_blocks = ["10.0.0.0/24"]
}

resource "yandex_vpc_subnet" "subnet-b" {
  name           = "subnet-b"
  zone           = var.yandex_zone
  network_id     = yandex_vpc_network.default.id
  route_table_id = yandex_vpc_route_table.nat.id
  v4_cidr_blocks = ["10.0.1.0/24"]
}

resource "yandex_vpc_subnet" "subnet-a" {
  name           = "subnet-a"
  zone           = var.var_zone_secondary
  network_id     = yandex_vpc_network.default.id
  route_table_id = yandex_vpc_route_table.nat.id
  v4_cidr_blocks = ["10.1.0.0/24"]
}

resource "yandex_vpc_address" "nginx-public-ip" {
  name = "public_ipv4"
  external_ipv4_address {
    zone_id = var.yandex_zone
  }
}

resource "yandex_vpc_route_table" "nat" {
  network_id = yandex_vpc_network.default.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    next_hop_address   = "10.0.0.10"
  }
}