variable "yandex_zone" {
  default = "ru-central1-b"
}

variable "var_zone_secondary" {
  default = "ru-central1-a"
}

data "yandex_compute_image" "ubuntu" {
  family = "ubuntu-2204-lts"
}