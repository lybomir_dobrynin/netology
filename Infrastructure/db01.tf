resource "yandex_compute_instance" "db01" {
  name                      = "db01"
  zone                      = var.var_zone_secondary
  hostname                  = "db01.nowiknowit.ru."
  allow_stopping_for_update = true

  lifecycle {
    ignore_changes = all
  }

  resources {
    cores  = 4
    memory = 4
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      image_id    = data.yandex_compute_image.ubuntu.id
      name        = "db01"
      type        = "network-nvme"
      size        = "20"
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet-a.id
    ip_address = "10.1.0.10"
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}