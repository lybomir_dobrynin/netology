resource "yandex_compute_instance" "db02" {
  name                      = "db02"
  zone                      = var.yandex_zone
  hostname                  = "db02.nowiknowit.ru."
  allow_stopping_for_update = true

  lifecycle {
    ignore_changes = all
  }

  resources {
    cores  = 4
    memory = 4
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      image_id    = data.yandex_compute_image.ubuntu.id
      name        = "db02"
      type        = "network-nvme"
      size        = "20"
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet-b.id
    ip_address = "10.0.1.10"
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}