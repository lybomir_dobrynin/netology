resource "yandex_compute_instance" "runner" {
  name                      = "runner"
  zone                      = var.var_zone_secondary
  hostname                  = "runner.nowiknowit.ru."
  allow_stopping_for_update = true

  lifecycle {
    ignore_changes = all
  }

  resources {
    cores  = 4
    memory = 4
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      image_id    = data.yandex_compute_image.ubuntu.id
      name        = "runner"
      type        = "network-nvme"
      size        = "10"
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet-a.id
    ip_address = "10.1.0.14"
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}