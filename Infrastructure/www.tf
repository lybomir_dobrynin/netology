resource "yandex_compute_instance" "nginx" {
  name                      = "www"
  zone                      = var.yandex_zone
  hostname                  = "nowiknowit.ru."
  allow_stopping_for_update = true

  lifecycle {
    ignore_changes = all
  }

  resources {
    cores  = 2
    memory = 2
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      image_id    = "fd8q9r5va9p64uhch83k"
      name        = "nginx"
      type        = "network-nvme"
      size        = "5"
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.default.id
    nat        = true
    nat_ip_address = yandex_vpc_address.nginx-public-ip.external_ipv4_address[0].address
    ip_address = "10.0.0.10"
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}