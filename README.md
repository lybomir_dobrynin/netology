# Дипломный практикум в YandexCloud
## IAM
- Создается сервисный аккаун sa с правами для управления ресурсами, key-файл сохраняется в проект Infrastructure
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/Screenshot_0.png?raw=true)
- Создается бакет
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/Screenshot_3.png?raw=true)
- Генерируется файл настроек провайдера для проекта Infrastructure с указанием бакета и key-файла
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/provide.png?raw=true)

## Infrastructure
- Terraform сконфигурирован и создание инфраструктуры посредством Terraform без дополнительных ручных действий. 
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/Screenshot_1.png?raw=true)
- Настроен workspace STAGING.
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/bucket.png?raw=true)
- В зоне настроены все А записи
```
[powershell] > "nowiknowit.ru","www.nowiknowit.ru","gitlab.nowiknowit.ru","grafana.nowiknowit.ru","alertmanager.nowiknowit.ru","prometheus.nowiknowit.ru" | Resolve-DnsName -Server 1.1.1.1 -Type A

Name                                           Type   TTL   Section    IPAddress
----                                           ----   ---   -------    ---------
nowiknowit.ru                                  A      200   Answer     130.193.55.35
www.nowiknowit.ru                              A      200   Answer     130.193.55.35
gitlab.nowiknowit.ru                           A      200   Answer     130.193.55.35
grafana.nowiknowit.ru                          A      200   Answer     130.193.55.35
alertmanager.nowiknowit.ru                     A      200   Answer     130.193.55.35
prometheus.nowiknowit.ru                       A      200   Answer     130.193.55.35
```
- Настроены все upstream для выше указанных URL.
- В браузере можно открыть любой из этих URL и увидеть ответ сервера.
- MySQL работает в режиме репликации Master/Slave.
- В кластере автоматически создаётся база данных c именем wordpress.
- В кластере автоматически создаётся пользователь wordpress с полными правами на базу wordpress и паролем wordpress.
- Виртуальная машина на которой установлен WordPress и Nginx.
- В браузере можно открыть URL https://www.nowiknowit.ru и увидеть главную страницу WordPress.
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/www.png?raw=true)
- Интерфейс Gitlab доступен по https://gitlab.nowiknowit.ru. Создан проект WP
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/gitlab.png?raw=true)
- Хост runner зарегистрирован в gitlab
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/gitlab4.png?raw=true)
- В свойствах CICD добавлена переменная, которая содержит в себе закрытый ключ
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/gitlab2.png?raw=true)
- В репозиторий добавлен файл сборки [ .gitlab-ci.yml](./Infrastructure/%20.gitlab-ci.yml)
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/gitlab1.png?raw=true)
- При любом коммите в репозиторий с WordPress и создании тега (например, v1.0.0) происходит деплой на виртуальную машину.
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/gitlab3.png?raw=true)
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/gitlab5.png?raw=true)
- На хосте с wordpress создается файл info.php
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/gitlab6.png?raw=true)
- Интерфейсы Prometheus, Alert Manager и Grafana доступены по https. На всех серверах установлен Node Exporter и его метрики доступны Prometheus.
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/prometheus.png?raw=true)
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/alertmanager.png?raw=true)
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/grafana1.png?raw=true)
>![dashboard](https://gitlab.com/lybomir_dobrynin/netology/-/raw/main/.img/grafana2.png?raw=true)
